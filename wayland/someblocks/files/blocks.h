// Modify this file to change what commands output to your statusbar,
// and recompile using the make command.
static const Block blocks[] = {
    /*Icon*/   /*Command*/    /*Update Interval*/    /*Update Signal*/
    {" ",      "/home/bin/sb-mediaplayer",                0, 15},
    {"",       "/home/bin/weather_NOAA",                600,  0},
    {"Mem: ",  "BLOCK_INSTANCE=mem  /home/bin/memory",    2,  0},
    {"Swap: ", "BLOCK_INSTANCE=swap /home/bin/memory",    2,  0},
    {"",       "/home/bin/vol sb",                        0, 10},
    {"",       "date '+%a, %d of %B %Y [%I:%M:%S %p]'",   1,  0},
};

// sets delimeter between status commands.
// NULL character ('\0') means no delimeter.
static char delim[] = " | ";
static unsigned int delimLen = 3;
