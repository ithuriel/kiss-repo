/* See LICENSE file for copyright and license details. */
struct rule {
	const char *devregex;
	const char *user;
	const char *group;
	int mode;
	const char *path;
	const char *cmd;
} rules[] = {

	// tty
	{ "console",       "root", "tty", 0600, NULL, NULL },
	{ "ptmx",          "root", "tty", 0666, NULL, NULL },
	{ "pty.*",         "root", "tty", 0660, NULL, NULL },
	{ "tty",           "root", "tty", 0666, NULL, NULL },
	{ "tty[0-9]",      "root", "tty", 0600, NULL, NULL },
	{ "tty[0-9][0-9]", "root", "tty", 0660, NULL, NULL },
	{ "vcs*[0-9].*",   "root", "tty", 0660, NULL, NULL },

	// serial
	{ "hvc.*",         "root", "uucp",   0660, NULL, NULL },
	{ "hvi.*",         "root", "uucp",   0660, NULL, NULL },
	{ "ircomm[0-9].*", "root", "uucp",   0660, NULL, NULL },
	{ "mwave",         "root", "uucp",   0660, NULL, NULL },
	{ "noz[0-9].*",    "root", "uucp",   0660, NULL, NULL },
	{ "ppox[0-9].*",   "root", "uucp",   0660, NULL, NULL },
	{ "rfcomm[0-9].*", "root", "uucp",   0660, NULL, NULL },
	{ "slamr",         "root", "tty",    0660, NULL, NULL },
	{ "slusb",         "root", "tty",    0660, NULL, NULL },
	{ "tty.*",         "root", "uucp",   0660, NULL, NULL },
	{ "ttyACM[0-9]",   "root", "dialup", 0660, NULL, NULL },
	{ "vport.*",       "root", "root",   0660, NULL, NULL },


	// mem
	{ "full",      "root", "root", 0666, NULL,     NULL                       },
	{ "hw_random", "root", "root", 0660, "=hwrng", "@/etc/smdev.d/processdev" },
	{ "hwrandom",  "root", "root", 0660, NULL,     "@/etc/smdev.d/processdev" },
	{ "kmem",      "root", "kmem", 0640, NULL,     "@/etc/smdev.d/processdev" },
	{ "mem",       "root", "kmem", 0640, NULL,     "@/etc/smdev.d/processdev" },
	{ "null",      "root", "root", 0666, NULL,     NULL                       },
	{ "nvram",     "root", "kmem", 0640, NULL,     "@/etc/smdev.d/processdev" },
	{ "port",      "root", "kmem", 0640, NULL,     "@/etc/smdev.d/processdev" },
	{ "random",    "root", "root", 0666, NULL,     "@/etc/smdev.d/processdev" },
	{ "urandom",   "root", "root", 0444, NULL,     "@/etc/smdev.d/processdev" },
	{ "zero",      "root", "root", 0666, NULL,     NULL                       },

	// input
	{ "input/.*",  "root", "input", 0660, "=input/", "@/etc/smdev.d/processdev" },
	{ "ts[0-9].+", "root", "root",  0640, "=input/", "@/etc/smdev.d/processdev" },

	// graphics
	{ "video.*",   "root", "video", 0660, NULL, "@/etc/smdev.d/processdev" },
	{ "vchiq",     "root", "video", 0660, NULL, "@/etc/smdev.d/processdev" },
	{ "vbi[0-9]",  "root", "video", 0660, NULL, "@/etc/smdev.d/processdev" },
	{ "cec[0-9]",  "root", "video", 0660, NULL, "@/etc/smdev.d/processdev" },
	{ "agpgart",   "root", "video", 0660, NULL, "@/etc/smdev.d/processdev" },
	{ "pmu",       "root", "video", 0660, NULL, "@/etc/smdev.d/processdev" },
	{ "fb[0-9].*", "root", "video", 0660, NULL, "@/etc/smdev.d/processdev" },
	{ "nvidia",    "root", "video", 0660, NULL, "@/etc/smdev.d/processdev" },
	{ "nvidiactl", "root", "video", 0660, NULL, "@/etc/smdev.d/processdev" },

	// dri
	{ "card[0-9].*", "root", "video", 0660, "=dri/", "@/etc/smdev.d/processdev" },
	{ "dri/.*",      "root", "video", 0660, "=dri/", "@/etc/smdev.d/processdev" },

	// usb
	{ "bus/usb/.*", "root", "root", 0664, NULL, "@/etc/smdev.d/processdev" },

	// printer
	{ "partport[0-9].*", "root", "lp", 0660, NULL,    "@/etc/smdev.d/processdev" },
	{ "partport_pc",     "root", "lp", 0660, NULL,    "@/etc/smdev.d/processdev" },
	{ "usb/lp[0-9].*",   "root", "lp", 0660, "=usb/", "@/etc/smdev.d/processdev" },
	{ "lp[0-9]*",        "root", "lp", 0660, NULL,    "@/etc/smdev.d/processdev" },
	{ "irlpt[0-9].*",    "root"," lp", 0660, NULL,    "@/etc/smdev.d/processdev" },

	// block
	{ "btibm.*",       "root", "disk",  0660, NULL, "@/etc/smdev.d/processdev" },
	{ "control",       "root", "disk",  0660, NULL, "@/etc/smdev.d/processdev" },
	{ "dm-.*",         "root", "disk",  0660, NULL, "@/etc/smdev.d/processdev" },
	{ "fd[0-9].*",     "root", "disk",  0660, NULL, "@/etc/smdev.d/processdev" },
	{ "gnbd.*",        "root", "disk",  0660, NULL, "@/etc/smdev.d/processdev" },
	{ "hd[a-z].*",     "root", "disk",  0660, NULL, "@/etc/smdev.d/processdev" },
	{ "loop[0-9].*",   "root", "disk",  0660, NULL, "@/etc/smdev.d/processdev" },
	{ "md.*",          "root", "disk",  0660, NULL, "@/etc/smdev.d/processdev" },
	{ "mmcblk[0-9].*", "root", "disk",  0660, NULL, "@/etc/smdev.d/processdev" },
	{ "mtd.*",         "root", "disk",  0660, NULL, "@/etc/smdev.d/processdev" },
	{ "ndb.*",         "root", "disk",  0660, NULL, "@/etc/smdev.d/processdev" },
	{ "nvme[0-9].*",   "root", "disk",  0660, NULL, "@/etc/smdev.d/processdev" },
	{ "ram[0-9].*",    "root", "disk",  0660, NULL, "@/etc/smdev.d/processdev" },
	{ "sd[a-z].*",     "root", "disk",  0660, NULL, "@/etc/smdev.d/processdev" },
	{ "sg.*",          "root", "disk",  0660, NULL, "@/etc/smdev.d/processdev" },
	{ "sr[0-9].*",     "root", "cdrom", 0660, NULL, "@/etc/smdev.d/processdev" },
	{ "vd.*",          "root", "disk",  0660, NULL, "@/etc/smdev.d/processdev" },

	// tape
	{ "ht[0-9].*",   "root", "tape", 0660, NULL, "@/etc/smdev.d/processdev" },
	{ "nht[0-9].*",  "root", "tape", 0660, NULL, "@/etc/smdev.d/processdev" },
	{ "npt[0-9].*",  "root", "tape", 0660, NULL, "@/etc/smdev.d/processdev" },
	{ "nst*[0-9].*", "root", "tape", 0660, NULL, "@/etc/smdev.d/processdev" },
	{ "pht[0-9].*",  "root", "tape", 0660, NULL, "@/etc/smdev.d/processdev" },
	{ "pt[0-9].*",   "root", "tape", 0660, NULL, "@/etc/smdev.d/processdev" },
	{ "st*[0-9].*",  "root", "tape", 0660, NULL, "@/etc/smdev.d/processdev" },

	// block-related
	{ "btrfs-control",  "root", "root", 0600, NULL,       "@/etc/smdev.d/processdev" },
	{ "loop-control",   "root", "disk", 0660, NULL,       "@/etc/smdev.d/processdev" },
	{ "mapper/.*",      "root", "root", 0600, "=mapper/", "@/etc/smdev.d/processdev" },
	{ "scsi.*/.*",      "root", "disk", 0660, NULL,       "@/etc/smdev.d/processdev" },
	{ "bdi.*/.*",       "root", "disk", 0660, NULL,       "@/etc/smdev.d/processdev" },
	{ "sch[0-9].*",     "root", "disk", 0660, NULL,       "@/etc/smdev.d/processdev" },
	{ "pg[0-9].*",      "root", "disk", 0660, NULL,       "@/etc/smdev.d/processdev" },
	{ "qft[0-9].*",     "root", "disk", 0660, NULL,       "@/etc/smdev.d/processdev" },
	{ "nqft[0-9].*",    "root", "disk", 0660, NULL,       "@/etc/smdev.d/processdev" },
	{ "zqft[0-9].*",    "root", "disk", 0660, NULL,       "@/etc/smdev.d/processdev" },
	{ "nzqft[0-9].*",   "root", "disk", 0660, NULL,       "@/etc/smdev.d/processdev" },
	{ "rawqft[0-9].*",  "root", "disk", 0660, NULL,       "@/etc/smdev.d/processdev" },
	{ "nrawqft[0-9].*", "root", "disk", 0660, NULL,       "@/etc/smdev.d/processdev" },
	{ "raw[0-9].*",     "root", "disk", 0660, "=raw/",    "@/etc/smdev.d/processdev" },
	{ "rawctl",         "root", "disk", 0660, "=raw/",    "@/etc/smdev.d/processdev" },
	{ "aoe.*",          "root", "disk", 0660, "=etherd/", "@/etc/smdev.d/processdev" },
	{ "bsg/.*",         "root", "disk", 0660, "=bsg/",    "@/etc/smdev.d/processdev" },

	// audio
	{ "adsp",        "root", "audio", 0660, "=snd/", "@/etc/smdev.d/processdev" },
	{ "audio",       "root", "audio", 0660, "=snd/", "@/etc/smdev.d/processdev" },
	{ "dsp",         "root", "audio", 0660, "=snd/", "@/etc/smdev.d/processdev" },
	{ "hpet",        "root", "audio", 0660, "=snd/", "@/etc/smdev.d/processdev" },
	{ "midi.*",      "root", "audio", 0660, "=snd/", "@/etc/smdev.d/processdev" },
	{ "mixer",       "root", "audio", 0660, "=snd/", "@/etc/smdev.d/processdev" },
	{ "pcm.*",       "root", "audio", 0660, "=snd/", "@/etc/smdev.d/processdev" },
	{ "seq",         "root", "audio", 0660, "=snd/", "@/etc/smdev.d/processdev" },
	{ "sequencer.*", "root", "audio", 0660, "=snd/", "@/etc/smdev.d/processdev" },
	{ "snd/.*",      "root", "audio", 0660, "=snd/", "@/etc/smdev.d/processdev" },
	{ "timer",       "root", "audio", 0660, "=snd/", "@/etc/smdev.d/processdev" },
	{ "timer",       "root", "audio", 0660, "=snd/", "@/etc/smdev.d/processdev" },

	// network
	{ "-net/.*",    "root", "network", 0660, NULL,    "@nameif"                  },
	{ "rfkill",     "root", "root",    0640, NULL,    "@/etc/smdev.d/processdev" },
	{ "tap[0-9].*", "root", "network", 0660, "=net/", "@/etc/smdev.d/processdev" },
	{ "tun",        "root", "network", 0666, "=net/", "@/etc/smdev.d/processdev" },
	{ "tun[0-9].*", "root", "network", 0660, "=net/", "@/etc/smdev.d/processdev" },

	// cpu
	{ "cpu([0-9].+)", "root", "root", 0444, "=cpu/%1/cpuid",  "@/etc/smdev.d/processdev" },
	{ "microcode",    "root", "root", 0600, "=cpu/microcode", "@/etc/smdev.d/processdev" },
	{ "msr([0-9].+)", "root", "root", 0600, "=cpu/%1/msr",    "@/etc/smdev.d/processdev" },

	// fuse
	{ "fuse",            "root", "root",  0666, NULL,      "@mount -t fusectl fusectl /sys/fs/fuse/connections"},

	// raid controllers
	{ "cciss.*", "root", "disk", 0660, NULL, "@/etc/smdev.d/processdev" },
	{ "ida.*",   "root", "disk", 0660, NULL, "@/etc/smdev.d/processdev" },
	{ "rd.*",    "root", "disk", 0660, NULL, "@/etc/smdev.d/processdev" },

	// misc
	{ "auer[0-9]*",   "root", "root",  0660, "=usb/",              "@/etc/smdev.d/processdev" },
	{ "kvm",          "root", "kvm",   0660, NULL,                 "@/etc/smdev.d/processdev" },
	{ "mmtimer",      "root", "root",  0644, NULL,                 "@/etc/smdev.d/processdev" },
	{ "ppp",          "root", "root",  0000, NULL,                 "@/etc/smdev.d/processdev" },
	{ "rflash[0-9]*", "root", "root",  0400, NULL,                 "@/etc/smdev.d/processdev" },
	{ "rioctl",       "root", "root",  0660, "=specialix_rioctl/", "@/etc/smdev.d/processdev" },
	{ "rrom[0-9]*",   "root", "root",  0400, NULL,                 "@/etc/smdev.d/processdev" },
	{ "rtc",          "root", "root",  0660, NULL,                 "@/etc/smdev.d/processdev" },
	{ "rtc[0-9]*",    "root", "root",  0664, NULL,                 "@/etc/smdev.d/processdev" },
	{ "sxctl",        "root", "root",  0660, "=specialix_sxctl/",  "@/etc/smdev.d/processdev" },
	{ "vhost-net",    "root", "kvm",   0666, NULL,                 "@/etc/smdev.d/processdev" },
	{ "vhost-vsock",  "root", "kvm",   0666, NULL,                 "@/etc/smdev.d/processdev" },

	// arch
	{ "iseries/ibmsis.*", "root", "disk", 0660, NULL, "@/etc/smdev.d/processdev" },
	{ "iseries/nvt.*",    "root", "disk", 0660, NULL, "@/etc/smdev.d/processdev" },
	{ "iseries/vt.*",     "root", "disk", 0660, NULL, "@/etc/smdev.d/processdev" },
	{ "iseries/vtty.*",   "root", "disk", 0660, NULL, "@/etc/smdev.d/processdev" },
	{ "sgi_.*",           "root", "root", 0666, NULL, "@/etc/smdev.d/processdev" },
	
	{ ".*",               "root", "root", 0660, NULL, "@/etc/smdev.d/processdev" },
};

/* Fill into this table if you want to rename the network interface
 * identified by `mac' to `name'.  By default no such renaming takes
 * place.
 */
struct mac2name {
	unsigned char mac[6];
	const char *name;
} mac2names[] = {
	{ .mac = { 0 }, .name = NULL }
};
